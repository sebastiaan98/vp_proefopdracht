﻿using UnityEngine;
using System.Collections;

public class EnemyMovement1 : MonoBehaviour
{
	private float timer;
	private Renderer rend;
	[SerializeField]
	private float movementSpeed = 10f;
	private Rigidbody rb;
    private int random;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
movementSpeed = Random.Range(1, 8);
        random = Random.Range(1, 8);
    }

     void Update()
    {
   		Vector2 movement = new Vector2(1, 0.0f);
        rb.velocity = movement * movementSpeed;
    }

    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject.tag == "Wall2")
        {
            movementSpeed *= -1;           
        }
    }
}