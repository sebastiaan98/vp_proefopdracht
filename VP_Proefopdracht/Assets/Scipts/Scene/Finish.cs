﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class Finish : MonoBehaviour {

	 void OnTriggerEnter (Collider other)
    {
        if(other.gameObject.name == "Player")
        {
           Debug.Log("Win");
            SceneManager.LoadScene(2);
        }
	}
}
